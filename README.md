# README #

Explaination with mathamatic. https://drive.google.com/file/d/1uD74YaAkmbqgFE_hn7GpBt_GM1S8zAJ8/view?usp=sharing

## git branch ##
* I recommend to checkout to level through these branch from the top to bottom
* feature/beginner
* feature/cleaning
* feature/unicode
* feature/demo


## What is this repository for? ##

This repo aims to share some knowledge about machine learning based on Naive Bayes Algorithm.
version 1.0

## How do I get set up? ##

* install python
* python NLP.py --mode train ## to train
* python NLP.py --mode chat ## to chat
#### unicode support ####
* for unicode support, you need to install some dependencies, here the guideline : http://niptict.edu.kh/khmer-word-segmentation-tool/
* you need to modify the path in Libs/Unicode/KhmerSegment.py
```
def __init__(self):
	self.PATH = '/Data/km-5tag-seg-1.0' # here the path of khmer segmentation lib that i download from niptict
```

* chatbot configuration
* you can find the config in NLP.py.
### Khmer Chatbot ###
```
config = {
	'dataset': 'db.khmer.json',
	'model'	 : 'data/naive_bayes_khmer_model.pickle',
	'mode'	 : 'unicode' # nonunicode or unicode
}
```
### English Chatbot ###
```
config = {
	'dataset': 'db.json',
	'model'	 : 'data/naive_bayes_model.pickle',
	'mode'	 : 'nonunicode' # nonunicode or unicode
}
```
## Who do I talk to? ##

if you have any question, you can send to this email kevin@slash.us.com.
