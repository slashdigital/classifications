
import os
import argparse
from Libs.MachineLearning import MachineLearning

def createDataFolder():
	if not os.path.exists('data'):
		os.makedirs('data')

def main():
	# save_conversation = SaveConversation()
	createDataFolder()

	parser = argparse.ArgumentParser()
	parser.add_argument('--mode',type=str,default ='chat',help='There are two mode (chat, train, train_c, test and none), The defaul value is chat.')
	# parser.add_argument("--benchmark", help="run benchmark",action="store_true")
	# parser.add_argument('--mode',type=float,default =0.2,help='There two mode?(train and chat)')
	args = parser.parse_args()

	config = {
		'dataset': 'db.khmer.json',
		'model'	 : 'data/naive_bayes_khmer_model.pickle',
		'mode'	 : 'unicode'
	}

	machine = MachineLearning(**config).NiaveBayes()

	# -- mode 
	if args.mode == 'train' :
		machine.loadDataset()
		machine.train()
	elif args.mode == 'chat':
		print ("Start chatting with the bot !")
		machine.loadModel()
		sessionid = 'Liza'
		while True:
			question 	= input('')
			machine.predict(question)
			# print(Bcolors().OKGREEN + 'You :' + Bcolors().ENDC,question)
			# print(Bcolors().OKBLUE 	+ 'Bot :' + Bcolors().ENDC,answer)
			# # save all chat to db
			# data = 'sessionid;;'+sessionid +';;question;;'+ question+ ';;response;;' + answer
			# save_conversation.save(data)
	elif args.mode == 'test':
		pass
	elif args.mode == 'none':
		from Libs.Unicode.UnicodeSplit import UnicodeSplit
		print('unicode split',UnicodeSplit().unicode_split('តើអ្នកអាចធ្វើVISAហ្នឹងអោយលឿនបានទេ'))


main()
